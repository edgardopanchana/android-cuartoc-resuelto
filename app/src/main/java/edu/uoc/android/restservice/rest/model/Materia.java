package edu.uoc.android.restservice.rest.model;

import com.google.gson.annotations.SerializedName;

public class Materia {

    @SerializedName("descripcion")
    private String descripcion;

    @SerializedName("parcial_uno")
    private String parcial_uno;

    @SerializedName("parcial_dos")
    private String parcial_dos;

    @SerializedName("suma")
    private String suma;

    @SerializedName("aprueba")
    private String aprueba;

    public String getDescripcion() {
        return descripcion;
    }

    public String getParcial_uno() {
        return parcial_uno;
    }

    public String getParcial_dos() {
        return parcial_dos;
    }

    public String getSuma() {
        return suma;
    }

    public String getAprueba() {
        return aprueba;
    }
}
