package edu.uoc.android.restservice.rest.model;

import com.google.gson.annotations.SerializedName;

public class Estudiante {

    @SerializedName("name")
    private String name;

    @SerializedName("last_name")
    private String last_name;

    @SerializedName("nivel")
    private String nivel;

    @SerializedName("materias_aprobadas")
    private String materias_aprobadas;

    @SerializedName("materias_reprobadas")
    private String materias_reprobadas;

    @SerializedName("imagen")
    private String imagen;


    public String getName() {
        return name;
    }

    public String getLastName() {
        return last_name;
    }

    public String getNivel() {
        return nivel;
    }

    public String getMaterias_aprobadas() {
        return materias_aprobadas;
    }

    public String getMaterias_reprobadas() {
        return materias_reprobadas;
    }

    public String getImagen() {
        return imagen;
    }
}
