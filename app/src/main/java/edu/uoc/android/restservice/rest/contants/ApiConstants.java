package edu.uoc.android.restservice.rest.contants;

public class ApiConstants {

    // BASE URL
    public static final String BASE_GITHUB_URL = "http://10.0.2.2:3002/";

    // ENDPOINTS
    public static final String GITHUB_USER_ENDPOINT = "estudiante/{cedula}";
    public static final String GITHUB_FOLLOWERS_ENDPOINT = "/materias/{cedula}";

    //http://localhost:3002/estudiante/1112223334
}
