package edu.uoc.android.restservice.ui.enter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import edu.uoc.android.restservice.R;
import edu.uoc.android.restservice.rest.adapter.GitHubAdapter;
import edu.uoc.android.restservice.rest.model.Estudiante;
import edu.uoc.android.restservice.rest.model.Materia;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class InfoUserActivity extends AppCompatActivity {

    ArrayList<Materia> listaFollowers;
    RecyclerView recyclerViewFollowers;

    TextView textViewNombre, textViewApellido, textViewNivel, textViewMateriasAprobadas, getTextViewMateriasReprobadas;
    ImageView imageViewProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info_user);

        textViewNombre = findViewById(R.id.textViewNombre);
        textViewApellido = findViewById(R.id.textViewApellido);
        textViewNivel = findViewById(R.id.textViewNivel);
        textViewMateriasAprobadas = findViewById(R.id.textViewMateriasAprobadas);
        getTextViewMateriasReprobadas = findViewById(R.id.textViewMateriasReprobadas);

        imageViewProfile = (ImageView) findViewById(R.id.imageViewProfile);

        listaFollowers = new ArrayList<>();
        recyclerViewFollowers = (RecyclerView)findViewById(R.id.recyclerViewFollowers);
        recyclerViewFollowers.setLayoutManager(new LinearLayoutManager(this));

        String cedula = getIntent().getStringExtra("cedula");
        mostrarDatosBasicos(cedula);
        mostrarSeguidores(cedula);
    }

    private void mostrarDatosBasicos(String loginName){
        GitHubAdapter adapter = new GitHubAdapter();

        Call<Estudiante> call = adapter.getOwner(loginName);

        call.enqueue(new Callback<Estudiante>() {

            @Override
            public void onResponse(Call<Estudiante> call, Response<Estudiante> response) {
                Estudiante owner = response.body();
                textViewNombre.setText(owner.getName().toString());
                textViewApellido.setText(owner.getLastName().toString());
                textViewNivel.setText(owner.getNivel().toString());
                textViewMateriasAprobadas.setText(owner.getMaterias_aprobadas().toString());
                getTextViewMateriasReprobadas.setText(owner.getMaterias_reprobadas().toString());
                Picasso.get().load(owner.getImagen()).into(imageViewProfile);
            }

            @Override
            public void onFailure(Call<Estudiante> call, Throwable t) {

            }
        });


        }

        private void mostrarSeguidores(String cedula){
            GitHubAdapter adapter = new GitHubAdapter();
            Call<List<Materia>> call = adapter.getOwnerFollowers(cedula);
            call.enqueue(new Callback<List<Materia>>() {
                @Override
                public void onResponse(Call<List<Materia>> call, Response<List<Materia>> response) {
                    List<Materia> lista = response.body();
                    for (Materia owner: lista
                         ) {
                        Log.e("LOGIN", owner.getDescripcion());
                        listaFollowers.add(owner);
                    }
                    AdaptadorFollowers adaptadorFollowers = new AdaptadorFollowers(listaFollowers);
                    recyclerViewFollowers.setAdapter(adaptadorFollowers);
                }

                @Override
                public void onFailure(Call<List<Materia>> call, Throwable t) {

                }
            });
        }
}
